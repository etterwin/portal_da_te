# Тестовое задание
Выполнено Плотниковой Д.А.

Требования:
- Node v.14.18.*
- Yarn

Если версия Node не соответствует заданному, то добавьте в начало команды запуска проекта (package.json:6) следующий код:
```bash
SET NODE_OPTIONS=--openssl-legacy-provider &&
```

Установить yarn
```bash
npm install -g yarn
```

## Запуск впервые
```bash
yarn start
```

## Установка зависимостей
```bash
yarn install
```

### Режим разработки
```bash
yarn dev
```

### Сборка проекта
```bash
yarn build
```


