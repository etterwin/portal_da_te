import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import Router from 'vue-router'
import vClickOutside from 'v-click-outside'
import axios from 'axios'

Vue.config.productionTip = false
Vue.use(vClickOutside)
Vue.use(Router)
Vue.use(Vuex)

const router = new Router(
  {
    path: '/',
    component: App
  }
)

const store = new Vuex.Store({
  state: {
    exchangeRate: []
  },
  getters: {
    getExchangeRate (state) {
      return state.exchangeRate
    }
  },
  mutations: {
    setExchangeRate (state, rate) {
      state.exchangeRate = rate
    }
  },
  actions: {
    async exchangeRateList ({commit}) {
      try {
        const rate = await axios.get('https://www.cbr-xml-daily.ru/daily_json.js')
        let result = []
        const rateList = rate.data.Valute

        for (let i in rateList) {
          result.push(
            {
              id: rateList[i].ID,
              currencyLetters: rateList[i].CharCode,
              multiplicity: rateList[i].Nominal,
              value: rateList[i].Value,
              unit: rateList[i].Name
            }
          )
        }

        commit('setExchangeRate', result)
      } catch (error) {
        console.log(error)
      }
    },
  }
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
